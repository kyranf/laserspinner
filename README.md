README FILE FOR LASERSPINNER REPO

Support and Queries go to kyfindlater@gmail.com

You may do commit/push requests of improvements and bug fixes. Make sure you say why!

Firmware was written in Arduino 1.0.4 and does not use any special libraries. 
Make sure to compile for the Arduino pro 16Mhz 5V board variant.

ROS node folder has an important README file in it, explaining installation/setup/environment issues and overview of the uses of the node.
Please read it. 

Have fun! - Kyran