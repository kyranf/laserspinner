/* Auto-generated by genmsg_cpp for file /home/cyphy/ros/laser_spinner/srv/laser_power.srv */
#ifndef LASER_SPINNER_SERVICE_LASER_POWER_H
#define LASER_SPINNER_SERVICE_LASER_POWER_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"

#include "ros/service_traits.h"




namespace laser_spinner
{
template <class ContainerAllocator>
struct laser_powerRequest_ {
  typedef laser_powerRequest_<ContainerAllocator> Type;

  laser_powerRequest_()
  : state(0)
  {
  }

  laser_powerRequest_(const ContainerAllocator& _alloc)
  : state(0)
  {
  }

  typedef uint16_t _state_type;
  uint16_t state;


  typedef boost::shared_ptr< ::laser_spinner::laser_powerRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::laser_spinner::laser_powerRequest_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct laser_powerRequest
typedef  ::laser_spinner::laser_powerRequest_<std::allocator<void> > laser_powerRequest;

typedef boost::shared_ptr< ::laser_spinner::laser_powerRequest> laser_powerRequestPtr;
typedef boost::shared_ptr< ::laser_spinner::laser_powerRequest const> laser_powerRequestConstPtr;


template <class ContainerAllocator>
struct laser_powerResponse_ {
  typedef laser_powerResponse_<ContainerAllocator> Type;

  laser_powerResponse_()
  : state(false)
  {
  }

  laser_powerResponse_(const ContainerAllocator& _alloc)
  : state(false)
  {
  }

  typedef uint8_t _state_type;
  uint8_t state;


  typedef boost::shared_ptr< ::laser_spinner::laser_powerResponse_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::laser_spinner::laser_powerResponse_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct laser_powerResponse
typedef  ::laser_spinner::laser_powerResponse_<std::allocator<void> > laser_powerResponse;

typedef boost::shared_ptr< ::laser_spinner::laser_powerResponse> laser_powerResponsePtr;
typedef boost::shared_ptr< ::laser_spinner::laser_powerResponse const> laser_powerResponseConstPtr;

struct laser_power
{

typedef laser_powerRequest Request;
typedef laser_powerResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;
}; // struct laser_power
} // namespace laser_spinner

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::laser_spinner::laser_powerRequest_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::laser_spinner::laser_powerRequest_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::laser_spinner::laser_powerRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "891b541ef99af7889d0f22a062410be8";
  }

  static const char* value(const  ::laser_spinner::laser_powerRequest_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0x891b541ef99af788ULL;
  static const uint64_t static_value2 = 0x9d0f22a062410be8ULL;
};

template<class ContainerAllocator>
struct DataType< ::laser_spinner::laser_powerRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "laser_spinner/laser_powerRequest";
  }

  static const char* value(const  ::laser_spinner::laser_powerRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::laser_spinner::laser_powerRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "uint16 state\n\
\n\
";
  }

  static const char* value(const  ::laser_spinner::laser_powerRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct IsFixedSize< ::laser_spinner::laser_powerRequest_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros


namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::laser_spinner::laser_powerResponse_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::laser_spinner::laser_powerResponse_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::laser_spinner::laser_powerResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "001fde3cab9e313a150416ff09c08ee4";
  }

  static const char* value(const  ::laser_spinner::laser_powerResponse_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0x001fde3cab9e313aULL;
  static const uint64_t static_value2 = 0x150416ff09c08ee4ULL;
};

template<class ContainerAllocator>
struct DataType< ::laser_spinner::laser_powerResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "laser_spinner/laser_powerResponse";
  }

  static const char* value(const  ::laser_spinner::laser_powerResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::laser_spinner::laser_powerResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "bool state\n\
\n\
\n\
";
  }

  static const char* value(const  ::laser_spinner::laser_powerResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct IsFixedSize< ::laser_spinner::laser_powerResponse_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::laser_spinner::laser_powerRequest_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.state);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct laser_powerRequest_
} // namespace serialization
} // namespace ros


namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::laser_spinner::laser_powerResponse_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.state);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct laser_powerResponse_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace service_traits
{
template<>
struct MD5Sum<laser_spinner::laser_power> {
  static const char* value() 
  {
    return "294e321e28e8f9b1fcbe554e71b4a94f";
  }

  static const char* value(const laser_spinner::laser_power&) { return value(); } 
};

template<>
struct DataType<laser_spinner::laser_power> {
  static const char* value() 
  {
    return "laser_spinner/laser_power";
  }

  static const char* value(const laser_spinner::laser_power&) { return value(); } 
};

template<class ContainerAllocator>
struct MD5Sum<laser_spinner::laser_powerRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "294e321e28e8f9b1fcbe554e71b4a94f";
  }

  static const char* value(const laser_spinner::laser_powerRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct DataType<laser_spinner::laser_powerRequest_<ContainerAllocator> > {
  static const char* value() 
  {
    return "laser_spinner/laser_power";
  }

  static const char* value(const laser_spinner::laser_powerRequest_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct MD5Sum<laser_spinner::laser_powerResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "294e321e28e8f9b1fcbe554e71b4a94f";
  }

  static const char* value(const laser_spinner::laser_powerResponse_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct DataType<laser_spinner::laser_powerResponse_<ContainerAllocator> > {
  static const char* value() 
  {
    return "laser_spinner/laser_power";
  }

  static const char* value(const laser_spinner::laser_powerResponse_<ContainerAllocator> &) { return value(); } 
};

} // namespace service_traits
} // namespace ros

#endif // LASER_SPINNER_SERVICE_LASER_POWER_H

