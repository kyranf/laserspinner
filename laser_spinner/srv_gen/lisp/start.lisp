; Auto-generated. Do not edit!


(cl:in-package laser_spinner-srv)


;//! \htmlinclude start-request.msg.html

(cl:defclass <start-request> (roslisp-msg-protocol:ros-message)
  ((speed
    :reader speed
    :initarg :speed
    :type cl:fixnum
    :initform 0)
   (direction
    :reader direction
    :initarg :direction
    :type cl:fixnum
    :initform 0))
)

(cl:defclass start-request (<start-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <start-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'start-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<start-request> is deprecated: use laser_spinner-srv:start-request instead.")))

(cl:ensure-generic-function 'speed-val :lambda-list '(m))
(cl:defmethod speed-val ((m <start-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-srv:speed-val is deprecated.  Use laser_spinner-srv:speed instead.")
  (speed m))

(cl:ensure-generic-function 'direction-val :lambda-list '(m))
(cl:defmethod direction-val ((m <start-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-srv:direction-val is deprecated.  Use laser_spinner-srv:direction instead.")
  (direction m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <start-request>) ostream)
  "Serializes a message object of type '<start-request>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'speed)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'speed)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'direction)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <start-request>) istream)
  "Deserializes a message object of type '<start-request>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'speed)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'speed)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'direction) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<start-request>)))
  "Returns string type for a service object of type '<start-request>"
  "laser_spinner/startRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'start-request)))
  "Returns string type for a service object of type 'start-request"
  "laser_spinner/startRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<start-request>)))
  "Returns md5sum for a message object of type '<start-request>"
  "3735235eab9757d553e0d64121416ec1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'start-request)))
  "Returns md5sum for a message object of type 'start-request"
  "3735235eab9757d553e0d64121416ec1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<start-request>)))
  "Returns full string definition for message of type '<start-request>"
  (cl:format cl:nil "uint16 speed~%int16 direction~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'start-request)))
  "Returns full string definition for message of type 'start-request"
  (cl:format cl:nil "uint16 speed~%int16 direction~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <start-request>))
  (cl:+ 0
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <start-request>))
  "Converts a ROS message object to a list"
  (cl:list 'start-request
    (cl:cons ':speed (speed msg))
    (cl:cons ':direction (direction msg))
))
;//! \htmlinclude start-response.msg.html

(cl:defclass <start-response> (roslisp-msg-protocol:ros-message)
  ((ok
    :reader ok
    :initarg :ok
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass start-response (<start-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <start-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'start-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<start-response> is deprecated: use laser_spinner-srv:start-response instead.")))

(cl:ensure-generic-function 'ok-val :lambda-list '(m))
(cl:defmethod ok-val ((m <start-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-srv:ok-val is deprecated.  Use laser_spinner-srv:ok instead.")
  (ok m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <start-response>) ostream)
  "Serializes a message object of type '<start-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ok) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <start-response>) istream)
  "Deserializes a message object of type '<start-response>"
    (cl:setf (cl:slot-value msg 'ok) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<start-response>)))
  "Returns string type for a service object of type '<start-response>"
  "laser_spinner/startResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'start-response)))
  "Returns string type for a service object of type 'start-response"
  "laser_spinner/startResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<start-response>)))
  "Returns md5sum for a message object of type '<start-response>"
  "3735235eab9757d553e0d64121416ec1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'start-response)))
  "Returns md5sum for a message object of type 'start-response"
  "3735235eab9757d553e0d64121416ec1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<start-response>)))
  "Returns full string definition for message of type '<start-response>"
  (cl:format cl:nil "bool ok~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'start-response)))
  "Returns full string definition for message of type 'start-response"
  (cl:format cl:nil "bool ok~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <start-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <start-response>))
  "Converts a ROS message object to a list"
  (cl:list 'start-response
    (cl:cons ':ok (ok msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'start)))
  'start-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'start)))
  'start-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'start)))
  "Returns string type for a service object of type '<start>"
  "laser_spinner/start")