; Auto-generated. Do not edit!


(cl:in-package laser_spinner-srv)


;//! \htmlinclude laser_power-request.msg.html

(cl:defclass <laser_power-request> (roslisp-msg-protocol:ros-message)
  ((state
    :reader state
    :initarg :state
    :type cl:fixnum
    :initform 0))
)

(cl:defclass laser_power-request (<laser_power-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <laser_power-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'laser_power-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<laser_power-request> is deprecated: use laser_spinner-srv:laser_power-request instead.")))

(cl:ensure-generic-function 'state-val :lambda-list '(m))
(cl:defmethod state-val ((m <laser_power-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-srv:state-val is deprecated.  Use laser_spinner-srv:state instead.")
  (state m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <laser_power-request>) ostream)
  "Serializes a message object of type '<laser_power-request>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'state)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'state)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <laser_power-request>) istream)
  "Deserializes a message object of type '<laser_power-request>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'state)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'state)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<laser_power-request>)))
  "Returns string type for a service object of type '<laser_power-request>"
  "laser_spinner/laser_powerRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'laser_power-request)))
  "Returns string type for a service object of type 'laser_power-request"
  "laser_spinner/laser_powerRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<laser_power-request>)))
  "Returns md5sum for a message object of type '<laser_power-request>"
  "294e321e28e8f9b1fcbe554e71b4a94f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'laser_power-request)))
  "Returns md5sum for a message object of type 'laser_power-request"
  "294e321e28e8f9b1fcbe554e71b4a94f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<laser_power-request>)))
  "Returns full string definition for message of type '<laser_power-request>"
  (cl:format cl:nil "uint16 state~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'laser_power-request)))
  "Returns full string definition for message of type 'laser_power-request"
  (cl:format cl:nil "uint16 state~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <laser_power-request>))
  (cl:+ 0
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <laser_power-request>))
  "Converts a ROS message object to a list"
  (cl:list 'laser_power-request
    (cl:cons ':state (state msg))
))
;//! \htmlinclude laser_power-response.msg.html

(cl:defclass <laser_power-response> (roslisp-msg-protocol:ros-message)
  ((state
    :reader state
    :initarg :state
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass laser_power-response (<laser_power-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <laser_power-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'laser_power-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<laser_power-response> is deprecated: use laser_spinner-srv:laser_power-response instead.")))

(cl:ensure-generic-function 'state-val :lambda-list '(m))
(cl:defmethod state-val ((m <laser_power-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-srv:state-val is deprecated.  Use laser_spinner-srv:state instead.")
  (state m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <laser_power-response>) ostream)
  "Serializes a message object of type '<laser_power-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'state) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <laser_power-response>) istream)
  "Deserializes a message object of type '<laser_power-response>"
    (cl:setf (cl:slot-value msg 'state) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<laser_power-response>)))
  "Returns string type for a service object of type '<laser_power-response>"
  "laser_spinner/laser_powerResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'laser_power-response)))
  "Returns string type for a service object of type 'laser_power-response"
  "laser_spinner/laser_powerResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<laser_power-response>)))
  "Returns md5sum for a message object of type '<laser_power-response>"
  "294e321e28e8f9b1fcbe554e71b4a94f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'laser_power-response)))
  "Returns md5sum for a message object of type 'laser_power-response"
  "294e321e28e8f9b1fcbe554e71b4a94f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<laser_power-response>)))
  "Returns full string definition for message of type '<laser_power-response>"
  (cl:format cl:nil "bool state~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'laser_power-response)))
  "Returns full string definition for message of type 'laser_power-response"
  (cl:format cl:nil "bool state~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <laser_power-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <laser_power-response>))
  "Converts a ROS message object to a list"
  (cl:list 'laser_power-response
    (cl:cons ':state (state msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'laser_power)))
  'laser_power-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'laser_power)))
  'laser_power-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'laser_power)))
  "Returns string type for a service object of type '<laser_power>"
  "laser_spinner/laser_power")