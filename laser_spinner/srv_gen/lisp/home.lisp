; Auto-generated. Do not edit!


(cl:in-package laser_spinner-srv)


;//! \htmlinclude home-request.msg.html

(cl:defclass <home-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass home-request (<home-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <home-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'home-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<home-request> is deprecated: use laser_spinner-srv:home-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <home-request>) ostream)
  "Serializes a message object of type '<home-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <home-request>) istream)
  "Deserializes a message object of type '<home-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<home-request>)))
  "Returns string type for a service object of type '<home-request>"
  "laser_spinner/homeRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'home-request)))
  "Returns string type for a service object of type 'home-request"
  "laser_spinner/homeRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<home-request>)))
  "Returns md5sum for a message object of type '<home-request>"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'home-request)))
  "Returns md5sum for a message object of type 'home-request"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<home-request>)))
  "Returns full string definition for message of type '<home-request>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'home-request)))
  "Returns full string definition for message of type 'home-request"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <home-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <home-request>))
  "Converts a ROS message object to a list"
  (cl:list 'home-request
))
;//! \htmlinclude home-response.msg.html

(cl:defclass <home-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass home-response (<home-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <home-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'home-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<home-response> is deprecated: use laser_spinner-srv:home-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <home-response>) ostream)
  "Serializes a message object of type '<home-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <home-response>) istream)
  "Deserializes a message object of type '<home-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<home-response>)))
  "Returns string type for a service object of type '<home-response>"
  "laser_spinner/homeResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'home-response)))
  "Returns string type for a service object of type 'home-response"
  "laser_spinner/homeResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<home-response>)))
  "Returns md5sum for a message object of type '<home-response>"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'home-response)))
  "Returns md5sum for a message object of type 'home-response"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<home-response>)))
  "Returns full string definition for message of type '<home-response>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'home-response)))
  "Returns full string definition for message of type 'home-response"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <home-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <home-response>))
  "Converts a ROS message object to a list"
  (cl:list 'home-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'home)))
  'home-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'home)))
  'home-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'home)))
  "Returns string type for a service object of type '<home>"
  "laser_spinner/home")