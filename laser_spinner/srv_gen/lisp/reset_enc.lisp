; Auto-generated. Do not edit!


(cl:in-package laser_spinner-srv)


;//! \htmlinclude reset_enc-request.msg.html

(cl:defclass <reset_enc-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass reset_enc-request (<reset_enc-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <reset_enc-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'reset_enc-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<reset_enc-request> is deprecated: use laser_spinner-srv:reset_enc-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <reset_enc-request>) ostream)
  "Serializes a message object of type '<reset_enc-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <reset_enc-request>) istream)
  "Deserializes a message object of type '<reset_enc-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<reset_enc-request>)))
  "Returns string type for a service object of type '<reset_enc-request>"
  "laser_spinner/reset_encRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'reset_enc-request)))
  "Returns string type for a service object of type 'reset_enc-request"
  "laser_spinner/reset_encRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<reset_enc-request>)))
  "Returns md5sum for a message object of type '<reset_enc-request>"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'reset_enc-request)))
  "Returns md5sum for a message object of type 'reset_enc-request"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<reset_enc-request>)))
  "Returns full string definition for message of type '<reset_enc-request>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'reset_enc-request)))
  "Returns full string definition for message of type 'reset_enc-request"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <reset_enc-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <reset_enc-request>))
  "Converts a ROS message object to a list"
  (cl:list 'reset_enc-request
))
;//! \htmlinclude reset_enc-response.msg.html

(cl:defclass <reset_enc-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass reset_enc-response (<reset_enc-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <reset_enc-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'reset_enc-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<reset_enc-response> is deprecated: use laser_spinner-srv:reset_enc-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <reset_enc-response>) ostream)
  "Serializes a message object of type '<reset_enc-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <reset_enc-response>) istream)
  "Deserializes a message object of type '<reset_enc-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<reset_enc-response>)))
  "Returns string type for a service object of type '<reset_enc-response>"
  "laser_spinner/reset_encResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'reset_enc-response)))
  "Returns string type for a service object of type 'reset_enc-response"
  "laser_spinner/reset_encResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<reset_enc-response>)))
  "Returns md5sum for a message object of type '<reset_enc-response>"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'reset_enc-response)))
  "Returns md5sum for a message object of type 'reset_enc-response"
  "d41d8cd98f00b204e9800998ecf8427e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<reset_enc-response>)))
  "Returns full string definition for message of type '<reset_enc-response>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'reset_enc-response)))
  "Returns full string definition for message of type 'reset_enc-response"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <reset_enc-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <reset_enc-response>))
  "Converts a ROS message object to a list"
  (cl:list 'reset_enc-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'reset_enc)))
  'reset_enc-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'reset_enc)))
  'reset_enc-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'reset_enc)))
  "Returns string type for a service object of type '<reset_enc>"
  "laser_spinner/reset_enc")