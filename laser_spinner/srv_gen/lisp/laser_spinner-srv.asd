
(cl:in-package :asdf)

(defsystem "laser_spinner-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "encodervalue" :depends-on ("_package_encodervalue"))
    (:file "_package_encodervalue" :depends-on ("_package"))
    (:file "home" :depends-on ("_package_home"))
    (:file "_package_home" :depends-on ("_package"))
    (:file "start" :depends-on ("_package_start"))
    (:file "_package_start" :depends-on ("_package"))
    (:file "laser_power" :depends-on ("_package_laser_power"))
    (:file "_package_laser_power" :depends-on ("_package"))
    (:file "stop" :depends-on ("_package_stop"))
    (:file "_package_stop" :depends-on ("_package"))
    (:file "reset_enc" :depends-on ("_package_reset_enc"))
    (:file "_package_reset_enc" :depends-on ("_package"))
  ))