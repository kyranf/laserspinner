; Auto-generated. Do not edit!


(cl:in-package laser_spinner-srv)


;//! \htmlinclude encodervalue-request.msg.html

(cl:defclass <encodervalue-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass encodervalue-request (<encodervalue-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <encodervalue-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'encodervalue-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<encodervalue-request> is deprecated: use laser_spinner-srv:encodervalue-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <encodervalue-request>) ostream)
  "Serializes a message object of type '<encodervalue-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <encodervalue-request>) istream)
  "Deserializes a message object of type '<encodervalue-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<encodervalue-request>)))
  "Returns string type for a service object of type '<encodervalue-request>"
  "laser_spinner/encodervalueRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'encodervalue-request)))
  "Returns string type for a service object of type 'encodervalue-request"
  "laser_spinner/encodervalueRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<encodervalue-request>)))
  "Returns md5sum for a message object of type '<encodervalue-request>"
  "3be920bd77b2270a58935e91ea43b1b3")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'encodervalue-request)))
  "Returns md5sum for a message object of type 'encodervalue-request"
  "3be920bd77b2270a58935e91ea43b1b3")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<encodervalue-request>)))
  "Returns full string definition for message of type '<encodervalue-request>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'encodervalue-request)))
  "Returns full string definition for message of type 'encodervalue-request"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <encodervalue-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <encodervalue-request>))
  "Converts a ROS message object to a list"
  (cl:list 'encodervalue-request
))
;//! \htmlinclude encodervalue-response.msg.html

(cl:defclass <encodervalue-response> (roslisp-msg-protocol:ros-message)
  ((enc
    :reader enc
    :initarg :enc
    :type cl:integer
    :initform 0))
)

(cl:defclass encodervalue-response (<encodervalue-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <encodervalue-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'encodervalue-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-srv:<encodervalue-response> is deprecated: use laser_spinner-srv:encodervalue-response instead.")))

(cl:ensure-generic-function 'enc-val :lambda-list '(m))
(cl:defmethod enc-val ((m <encodervalue-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-srv:enc-val is deprecated.  Use laser_spinner-srv:enc instead.")
  (enc m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <encodervalue-response>) ostream)
  "Serializes a message object of type '<encodervalue-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'enc)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'enc)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'enc)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'enc)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <encodervalue-response>) istream)
  "Deserializes a message object of type '<encodervalue-response>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'enc)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'enc)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'enc)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'enc)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<encodervalue-response>)))
  "Returns string type for a service object of type '<encodervalue-response>"
  "laser_spinner/encodervalueResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'encodervalue-response)))
  "Returns string type for a service object of type 'encodervalue-response"
  "laser_spinner/encodervalueResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<encodervalue-response>)))
  "Returns md5sum for a message object of type '<encodervalue-response>"
  "3be920bd77b2270a58935e91ea43b1b3")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'encodervalue-response)))
  "Returns md5sum for a message object of type 'encodervalue-response"
  "3be920bd77b2270a58935e91ea43b1b3")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<encodervalue-response>)))
  "Returns full string definition for message of type '<encodervalue-response>"
  (cl:format cl:nil "uint32 enc~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'encodervalue-response)))
  "Returns full string definition for message of type 'encodervalue-response"
  (cl:format cl:nil "uint32 enc~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <encodervalue-response>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <encodervalue-response>))
  "Converts a ROS message object to a list"
  (cl:list 'encodervalue-response
    (cl:cons ':enc (enc msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'encodervalue)))
  'encodervalue-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'encodervalue)))
  'encodervalue-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'encodervalue)))
  "Returns string type for a service object of type '<encodervalue>"
  "laser_spinner/encodervalue")