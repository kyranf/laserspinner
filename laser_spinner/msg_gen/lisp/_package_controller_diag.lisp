(cl:in-package laser_spinner-msg)
(cl:export '(HEADER-VAL
          HEADER
          POWER_GOOD-VAL
          POWER_GOOD
          LASER_STATE-VAL
          LASER_STATE
          MOTOR_STATE-VAL
          MOTOR_STATE
          DIRECTION-VAL
          DIRECTION
          PERIPH_READY-VAL
          PERIPH_READY
          SYNC_SKIP-VAL
          SYNC_SKIP
          HOME_FAILED-VAL
          HOME_FAILED
          SPEED_CLICKS_PER_SEC-VAL
          SPEED_CLICKS_PER_SEC
          SPEED_RPM-VAL
          SPEED_RPM
))