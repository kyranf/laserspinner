
(cl:in-package :asdf)

(defsystem "laser_spinner-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "controller_diag" :depends-on ("_package_controller_diag"))
    (:file "_package_controller_diag" :depends-on ("_package"))
  ))