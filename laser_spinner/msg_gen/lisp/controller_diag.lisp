; Auto-generated. Do not edit!


(cl:in-package laser_spinner-msg)


;//! \htmlinclude controller_diag.msg.html

(cl:defclass <controller_diag> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (power_good
    :reader power_good
    :initarg :power_good
    :type cl:string
    :initform "")
   (laser_state
    :reader laser_state
    :initarg :laser_state
    :type cl:string
    :initform "")
   (motor_state
    :reader motor_state
    :initarg :motor_state
    :type cl:string
    :initform "")
   (direction
    :reader direction
    :initarg :direction
    :type cl:string
    :initform "")
   (periph_ready
    :reader periph_ready
    :initarg :periph_ready
    :type cl:string
    :initform "")
   (sync_skip
    :reader sync_skip
    :initarg :sync_skip
    :type cl:string
    :initform "")
   (home_failed
    :reader home_failed
    :initarg :home_failed
    :type cl:boolean
    :initform cl:nil)
   (speed_clicks_per_sec
    :reader speed_clicks_per_sec
    :initarg :speed_clicks_per_sec
    :type cl:fixnum
    :initform 0)
   (speed_RPM
    :reader speed_RPM
    :initarg :speed_RPM
    :type cl:float
    :initform 0.0))
)

(cl:defclass controller_diag (<controller_diag>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <controller_diag>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'controller_diag)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name laser_spinner-msg:<controller_diag> is deprecated: use laser_spinner-msg:controller_diag instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:header-val is deprecated.  Use laser_spinner-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'power_good-val :lambda-list '(m))
(cl:defmethod power_good-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:power_good-val is deprecated.  Use laser_spinner-msg:power_good instead.")
  (power_good m))

(cl:ensure-generic-function 'laser_state-val :lambda-list '(m))
(cl:defmethod laser_state-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:laser_state-val is deprecated.  Use laser_spinner-msg:laser_state instead.")
  (laser_state m))

(cl:ensure-generic-function 'motor_state-val :lambda-list '(m))
(cl:defmethod motor_state-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:motor_state-val is deprecated.  Use laser_spinner-msg:motor_state instead.")
  (motor_state m))

(cl:ensure-generic-function 'direction-val :lambda-list '(m))
(cl:defmethod direction-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:direction-val is deprecated.  Use laser_spinner-msg:direction instead.")
  (direction m))

(cl:ensure-generic-function 'periph_ready-val :lambda-list '(m))
(cl:defmethod periph_ready-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:periph_ready-val is deprecated.  Use laser_spinner-msg:periph_ready instead.")
  (periph_ready m))

(cl:ensure-generic-function 'sync_skip-val :lambda-list '(m))
(cl:defmethod sync_skip-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:sync_skip-val is deprecated.  Use laser_spinner-msg:sync_skip instead.")
  (sync_skip m))

(cl:ensure-generic-function 'home_failed-val :lambda-list '(m))
(cl:defmethod home_failed-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:home_failed-val is deprecated.  Use laser_spinner-msg:home_failed instead.")
  (home_failed m))

(cl:ensure-generic-function 'speed_clicks_per_sec-val :lambda-list '(m))
(cl:defmethod speed_clicks_per_sec-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:speed_clicks_per_sec-val is deprecated.  Use laser_spinner-msg:speed_clicks_per_sec instead.")
  (speed_clicks_per_sec m))

(cl:ensure-generic-function 'speed_RPM-val :lambda-list '(m))
(cl:defmethod speed_RPM-val ((m <controller_diag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader laser_spinner-msg:speed_RPM-val is deprecated.  Use laser_spinner-msg:speed_RPM instead.")
  (speed_RPM m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <controller_diag>) ostream)
  "Serializes a message object of type '<controller_diag>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'power_good))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'power_good))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'laser_state))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'laser_state))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'motor_state))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'motor_state))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'direction))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'direction))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'periph_ready))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'periph_ready))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'sync_skip))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'sync_skip))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'home_failed) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'speed_clicks_per_sec)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'speed_RPM))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <controller_diag>) istream)
  "Deserializes a message object of type '<controller_diag>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'power_good) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'power_good) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'laser_state) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'laser_state) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'motor_state) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'motor_state) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'direction) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'direction) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'periph_ready) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'periph_ready) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'sync_skip) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'sync_skip) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:slot-value msg 'home_failed) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'speed_clicks_per_sec) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'speed_RPM) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<controller_diag>)))
  "Returns string type for a message object of type '<controller_diag>"
  "laser_spinner/controller_diag")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'controller_diag)))
  "Returns string type for a message object of type 'controller_diag"
  "laser_spinner/controller_diag")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<controller_diag>)))
  "Returns md5sum for a message object of type '<controller_diag>"
  "dc232b67521eeddd9c7e428e8599b4d0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'controller_diag)))
  "Returns md5sum for a message object of type 'controller_diag"
  "dc232b67521eeddd9c7e428e8599b4d0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<controller_diag>)))
  "Returns full string definition for message of type '<controller_diag>"
  (cl:format cl:nil "Header header~%string power_good~%string laser_state~%string motor_state~%string direction~%string periph_ready~%string sync_skip~%bool home_failed~%int16 speed_clicks_per_sec~%float32 speed_RPM~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'controller_diag)))
  "Returns full string definition for message of type 'controller_diag"
  (cl:format cl:nil "Header header~%string power_good~%string laser_state~%string motor_state~%string direction~%string periph_ready~%string sync_skip~%bool home_failed~%int16 speed_clicks_per_sec~%float32 speed_RPM~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <controller_diag>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'power_good))
     4 (cl:length (cl:slot-value msg 'laser_state))
     4 (cl:length (cl:slot-value msg 'motor_state))
     4 (cl:length (cl:slot-value msg 'direction))
     4 (cl:length (cl:slot-value msg 'periph_ready))
     4 (cl:length (cl:slot-value msg 'sync_skip))
     1
     2
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <controller_diag>))
  "Converts a ROS message object to a list"
  (cl:list 'controller_diag
    (cl:cons ':header (header msg))
    (cl:cons ':power_good (power_good msg))
    (cl:cons ':laser_state (laser_state msg))
    (cl:cons ':motor_state (motor_state msg))
    (cl:cons ':direction (direction msg))
    (cl:cons ':periph_ready (periph_ready msg))
    (cl:cons ':sync_skip (sync_skip msg))
    (cl:cons ':home_failed (home_failed msg))
    (cl:cons ':speed_clicks_per_sec (speed_clicks_per_sec msg))
    (cl:cons ':speed_RPM (speed_RPM msg))
))
