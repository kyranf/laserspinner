
DOCUMENTATION: Laser Spin Controller ROS Driver Stack
-----------------------------------------------------

The contents of this archive/folder are the Laser Spinner Control drivers which
utilize ROS and Serial Port access to initialize, control, and monitor the rot-
-ation of a sensor (specifically a Hokuyo 30m LIDAR "UTM-30LX" or similar) for
the purpose of mobile robotic sensing and mapping, by way of interacting with
an embedded controller device (custom PCB, hardware). 


AUTHOR
------

Kyran Findlater 
March->June 2014
Email: kyfindlater@gmail.com ; or if still at QUT: k.findlater@qut.edu.au


README LAST UPDATED
-------------------

23/06/2014

ROS ENVIRONMENT
---------------

The testing of this driver was done solely on Ubuntu 12.04 LTS with ROS Groovy
installed for desktop development and run on the Husky using ROS Fuerte. Other
than the Hokuyo laser scanner drivers, this software package relies only on
standard ROS dependancies, such as TF and Roscpp etc. The stack initially was
built via Catkin processes, but later changed back to the old Rosbuild/Rosmake
system because the author was more familiar with that, and it was simpler. Also
the robot ROS distro did not yet support Catkin (Groovy+ does, robot is Fuerte!)


IMPORTANT FILES
---------------

The following files are the important/implemented files in the package:

    - manifest.xml: This is the package description and dependencies list
    - CMakeLists.txt: List which executables are made, and generate services
                      and messages.
    - srv/ folder contents: custom service definition files
    - msg/ folder contents: custom message definition file
    - src/controller.cpp: main serial controller for embedded device and ROS
                            node.
    - src/laser_startup_and_tf.cpp: init laser to start conditions in ROS launch
                                   file, and act as TF broadcaster afterwards
    - launchfiles/start_laser.launch: sets parameters, launches all nodes in
                                    the driver with node arguments and the
                                    required transforms/frame names etc.

                                    
HOW TO INSTALL
--------------
Unpack the archive into a valid ROS package path, and after adding a new package
to a ROS environment, according to the Wiki for ROSBuild adding packages run the 
following in terminal:

rospack profile
rospack find laser_spinner 


should return:
    YOUR_PACKAGE_PATH/laser_spinner
                           
To ensure it will work on your system, it's best to just call (and install if you
need it) rosdep, and then call in a terminal:

rosdep install laser_spinner

Next you should build the package natively so you can see if there are any 
errors or missing libraries, incompatible/old code etc. Call the following 
in the package's folder (folder with CMakeLists.txt there) with the --pre-clean 
argument:

rosmake --pre-clean
 
If calling from another folder, you can always use:

rosmake laser-spinner --pre-clean

After the build process there should be two executable/rosrun-able nodes, 
"controller" and "laser_startup_and_tf" using rosrun. be aware they may 
need arguments from commandline when calling, so check the launchfile for
example arguments! See section below for more details.

                        
HOW TO RUN THE NODE
-------------------

Assuming you have a correctly set up ROS environment, and ROSCORE is running you 
can launch the whole driver by using the ROSlaunch file in the /launchfiles 
folder. This is the easiest way, and most complete way to launch the driver. 
Without doing it this way, you will need to supply all the arguments to the nodes
yourself, and also set up the Hokuyo parameters externally. Best way to do it
if manually launching each node, is to read the launchfile and see what parameters
are normally passed to it, and other things that happen such as the hokuyo 
rosparam settings for the hokuyo node.

in most cases, after you have set up the launchfile with the appropriate parameters
for orientation and position of the rotating assembly with respect to the /base_link
position, and the correct frame names you will use as arguments, you should then call:

roslaunch laser_spinner start_laser.launch


If you want to launch them separately, should be done via rosrun. For example:

rosrun laser_spinner controller [args]
rosrun laser_spinner laser_startup_and_tf [args]

Even if you use the launchfiles, you must still also run the hokuyo node:

rosrun hokuyo_node hokuyo_node

If you have the laser_startup_and_tf node running, you will not be able to make
calls to the ROS service "Stop" directly, because this node will detect it as
a failure and attempt to start up the motor again. You can disable this in the 
software code by changing/commenting out the variable "recovermode = true" which
is called in Main() after the auto-setup process is complete. [last known to be line 624]

                                    
SERIAL PORT ACCESS
------------------

Serial port access for communications with the embedded controller is achieved
by USB -> Serial adapters, for full RS232 level interface. The driver can work
with COM ports, but must be modified to look at /dev/ttyxxxx where the xxxx is
given as an argument at command line or via ROS Launch file, such as 'USB0' or 'USB1'.

Serial communication protocol and configuration can be examined in the 
"Comms Protocol" Document. 


TOPICS PUBLISHED
----------------

By doing a call to "rosnode info controller" and "rosnode info laser_startup_and_tf"
you should be able to see the topics subscribed to, and published, by each node.
The controller node also advertises many services, to allow 3rd party drivers
to interact with the system. 

The Controller node publishes the following:
    Encoder data - topic name: /laser_encoder
    Diagnostics Data - topic name: /laser_control_diag

    
The Controller node also hosts the following ROS Services:
    start: "controller/start"      
    stop: "controller/stop"
    home: "controller/home"
    reset_enc: "controller/reset_enc"
    encodervalue: "controller/encodervalue"
    laser_power: "controller/laser_power"

    Any other ROS node in the system can make use of these services, thus allowing
    custom drivers and control over the embedded system.
    
The Startup and TF node publishes the following:
    Transform data - orientation of sensor [NOT A TOPIC, BUT A TF BROADCASTER]
    
    This node subscribes to:
        Encoder data - topic name: /laser_encoder
        Diagnostics Data - topic name: /laser_control_diag
        

GENERAL DESCRIPTION OF OPERATION
--------------------------------

The embedded controller which drives the motor and therefore rotates the sensor,
interacts with the ROS system and these drivers by means of serial communications.
The "Controller" node is the main Serial I/O interface and Service/ROS Topic layer
for the driver. The secondary node "Startup and TF" deals with smart feed-back
based initialization procedure, and then deals with the encoder -> orientation of
sensor co-ordinate transforms.           
        
        
VISUALIZING DATA
----------------

One way to visualize the data is to use Rviz. Launch Rviz and select "top_of_pole"
or some other fixed frame to be the reference, and then add the Laser Scan type to 
the interface. Select the /base_scan or whatever shows up as the laser scan data 
topic and the transforms should be mapping the laser orientation changes between 
the laser scan data and the base link of the robot.


EMBEDDED CONTROLLER CONNECTIONS
-------------------------------

The USB to serial converter plugs into the DB9 connection from the embedded 
controller, and the board must also be powered with 12V externally (from a 
battery or supply on the robot). There is on-board power regulation and 
fuses etc.

The connections from embedded system to the computer are simply 12V and GND
connections for power(1-2A max), the RS232 cable with TX, RX and again
a GND connection., finally the USB connection from the Hokuyo, which comes through
the slip-ring. The RS232 connection should be terminated with a DB9 header.

On the embedded controller itself, there are connections to:

- Motor power/windings/hall effect sensor feedback (8 pins/wires)
- Motor encoder cable (5 pins/wires)
- Light Dependent Resistor (LDR) (2 wires)
- Hokuyo 4-pin robot cable, through the slip-ring. (3 wires: 12V, GND, Sync)



EMBEDDED CONTROLLER OVERVIEW
----------------------------

The embedded controller acts as the motor driver, encoder feedback, and other control
functions for the robot to use the sensor appropriately. The onboard controller 
receives data from the serial interface, and acts as required. A simple use case is to
turn on power to the top rotating area (powers the laser, and Home LED) and using the 
LED and LDR to seek for home, reset the onboard encoder value, and beging rotating to
reach a set RPM. The controller node provides feedback such as the running time of the
controller microprocessor, the status of certain parts of the system - such as laser power,
motors on or off, if the input voltage is too low, and if it has failed to find home after 
a call to that function.





        