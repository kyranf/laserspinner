/*
    Controller for Laser Spinner Control board - This is a firmware/driver for an embedded system interface to PC
    Author: Kyran Findlater
    Contact: kyfindlater@gmail.com or k.findlater@qut.edu.au
    Date: March 2014
    
    Description:
    
    This program is responsible for acting as the ROS driver/interface to the embedded controller used
    to control the spin rate and encoder feedback of a rotating Hokuyo LIDAR assembly.
    
    The node will create a serial link with the controller, and allow other ROS programs or a user to
    control the laser assembly - powering it on/off, spin rate, spin direction, and a stream of feedback
    triggered by the laser sync pulse during operation. If the laser is not powered and taking samples,
    then the sync pulse will cause the embedded controller to stream data at a set rate. 

*/
#define _USE_MATH_DEFINES
#include <math.h>
#include "ros/ros.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>
#include <termios.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>


//include resources such as messages and custom service definitions
#include "std_msgs/UInt32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Header.h"
#include "laser_spinner/controller_diag.h"
#include "laser_spinner/laser_power.h"
#include "laser_spinner/start.h"
#include "laser_spinner/stop.h"
#include "laser_spinner/reset_enc.h"
#include "laser_spinner/home.h"
#include "laser_spinner/encodervalue.h"


 //status flag bit positions
#define PWR_GOOD 0 //if set, the voltage on vsys is good (above 5.5V which includes schottky diode drop)
#define PER_RDY 1 //peripherals ready/configured 
#define LASER_STATE 2 //laser power state
#define MOTOR_STATE 3 //motor enable state
#define MOTOR_DIR 4 //motor direction flag, same as the status flag on the LS7366
#define SYNC_SKIP 5 //if a sync pulse is detected before previous one finished
#define HOME_FAILED 6//if home fails by timeout, the controller lets us know

#define UP_IS_LEFT 1


int serialport;

struct termios options;
std::string fullpath =  "/dev/tty";
std::string defaultUSBname = "USB1";
std::vector<unsigned char> rxbuffer;

struct timespec current_time, last_beat, last_resp;
long timediff;

const long NANOSEC_PER_MILLISEC = 1e6;
const int beat_rate = 200;// milliseconds
const int beat_rate_timeout = 600;// milliseconds

const uint32_t clicks_per_rev = 29529; // gearbox is 3249/169 ratio, and pulley is 3/1. times the 512 counts per rev on the encoder shaft!
const double rot_per_click = (2*M_PI)/clicks_per_rev;


int head1_found = 0;
int head2_found = 0;
int message_started = 0;
int in_sync = 0;
static uint32_t msgseq = 0;


char numbuffer[5];
char pathbuffer[120];

uint32_t lastknownencoder = 0;


void send_heartbeat(void); //function prototype
uint8_t checksum(uint8_t* writebuffer, int indexToSum); //function prototype

//function prototype
void pack_message(struct serialmessage *messagestruct);

struct serialmessage {

	uint8_t header1;
	uint8_t header2;
	uint8_t len;
	uint8_t flags;	
	uint32_t encoder;
	uint16_t speed;
	uint32_t time;	
	uint8_t checksum;

};

struct blank_command {

    uint8_t size_of_message;
    
    uint8_t header1;
    uint8_t header2;
    uint8_t type;
    uint8_t len;
    uint8_t checksum;
    
};

struct power_command {

    uint8_t size_of_message;
    
    uint8_t header1;
    uint8_t header2;
    uint8_t type;
    uint8_t len;
    uint8_t state;
    uint8_t checksum;
    
};

struct start_command {

    uint8_t size_of_message;
    
    uint8_t header1;
    uint8_t header2;
    uint8_t type;
    uint8_t len;
    uint16_t speed;
    uint8_t dir;
    uint8_t checksum;
    
};


//whole bunch of service callback functions!

//TODO try-catch for each of the WriteByte clals, because writebyte throws exception if serial link is lost
bool start(laser_spinner::start::Request  &req,
         laser_spinner::start::Response &res)
    {
        //deal with the request data, and package up a serial write-buffer with the command
        //to start the laser motor, with direction and speed indicated
        
        uint8_t message[9];
        
        message[0] = 0xFF; //extern to board header 1
        message[1] = 0xFA; //extern to board header 2
        message[2] = 0x15; //start type byte
        message[3] = 0x03; //length of data, in terms of parameters
        message[4] = (req.speed >> 8); //upper half of 16 bits speed value
        message[5] = (req.speed); //lower half of 16 bits speed value
        message[6] = (req.direction);
        message[7] = checksum(message, 7);
        message[8] = '\0'; //null string terminator
        
        //send out the bytes
        for(int i = 0; i < 9; i++){
			
	        if(write(serialport, (message + i),1)){
			}else{
				ROS_ERROR("FAILED TO WRITE BYTE!");
			}
	        
        }
        res.ok = true;
        return true;
    }

bool stop(laser_spinner::stop::Request  &req,
         laser_spinner::stop::Response &res)
    {
    
        //deal with the request, package up serial write-buffer and 
        //stop the motor from spinning the laser.
        
        uint8_t message[6];
        
        message[0] = 0xFF; //extern to board header 1
        message[1] = 0xFA; //extern to board header 2
        message[2] = 0x0E; //stop type byte
        message[3] = 0x00; //length of data, in terms of parameters
        message[4] = checksum(message, 4);
        message[5] = '\0'; //null string terminator
        
        //send out the bytes
        for(int i = 0; i < 6; i++){
	        if(write(serialport, (message + i),1)){
			}else{
				ROS_ERROR("FAILED TO WRITE BYTE!");
			}
        }
        
        return true;
    }

bool laser_power(laser_spinner::laser_power::Request  &req,
         laser_spinner::laser_power::Response &res)
    {
        
        //deal with request data, set laser power on/off as requested
        //by preparing and sending serial buffer message
        
        uint8_t message[7];
        
        message[0] = 0xFF; //extern to board header 1
        message[1] = 0xFA; //extern to board header 2
        message[2] = 0x11; //start type byte
        message[3] = 0x01; //length of data, in terms of parameters
        message[4] = (req.state);
        message[5] = checksum(message, 5);
        message[6] = '\0'; //null string terminator
        
        //send out the bytes
        for(int i = 0; i < 7; i++){
	        if(write(serialport, (message + i),1)){
			}else{
				ROS_ERROR("FAILED TO WRITE BYTE!");
			}
        }
        res.state = req.state;
        
        return true;
    }


bool reset_enc(laser_spinner::reset_enc::Request  &req,
         laser_spinner::reset_enc::Response &res)
    {
        //deal with request, send serial message to reset encoder value
        
        uint8_t message[6];
        
        message[0] = 0xFF; //extern to board header 1
        message[1] = 0xFA; //extern to board header 2
        message[2] = 0x05; //reset type byte
        message[3] = 0x00; //length of data, in terms of parameters
        message[4] = checksum(message, 4);
        message[5] = '\0'; //null string terminator
        
        //send out the bytes
        for(int i = 0; i < 6; i++){
	        if(write(serialport, (message + i),1)){
			}else{
				ROS_ERROR("FAILED TO WRITE BYTE!");
			}
        }
        return true;
    }


bool encodervalue(laser_spinner::encodervalue::Request  &req,
         laser_spinner::encodervalue::Response &res)
    {
        //deal with request, return locally stored last known encoder value
        //no serial commands send here.
        if(in_sync){
            res.enc = lastknownencoder;
            return true;
        }
        
        ROS_WARN("controller is not in sync, or system has not moved since initializing (value is zero).");
        return false;
    }

bool home(laser_spinner::home::Request  &req,
         laser_spinner::home::Response &res)
    {
        //deal with request, send serial message to seek home
        uint8_t message[6];
        
        message[0] = 0xFF; //extern to board header 1
        message[1] = 0xFA; //extern to board header 2
        message[2] = 0x02; //home type byte
        message[3] = 0x00; //length of data, in terms of parameters
        message[4] = checksum(message, 4);
        message[5] = '\0'; //null string terminator
        
        //send out the bytes
        for(int i = 0; i < 6; i++){
	        if(write(serialport, (message + i),1)){
			}else{
				ROS_ERROR("FAILED TO WRITE BYTE!");
			}
        }
       
        return true;
    }

//This function is used to handle the Control-C event from terminal
void terminationHandler(int sig){
	
	printf("\nLaser Spinner ROS Node is shutting down.. sending serial message to stop motors!\n");

	uint8_t message[6];
 
	message[0] = 0xFF; //extern to board header 1
	message[1] = 0xFA; //extern to board header 2
	message[2] = 0x0E; //stop type byte
	message[3] = 0x00; //length of data, in terms of parameters
	message[4] = checksum(message, 4);
	message[5] = '\0'; //null string terminator
    
    
    int num_tries = 2;
    ros::Rate retry_sleep(1);   
    while(num_tries != 0)   {
        //send out the bytes
        for(int i = 0; i < 6; i++){
			write(serialport, (message + i),1);
        }
        retry_sleep.sleep();
        printf("\n sent stop command...");
        num_tries--;
	}
	
	printf("\n ROS Node is shutting down..\n");
	
	close(serialport);
	ros::shutdown();
	
}//end if sigint handling function



// MAIN PROGRAM //

int main(int argc, char **argv){

	ros::init(argc, argv, "controller");
	ros::NodeHandle n;
	
	signal(SIGINT, terminationHandler);
	ros::Publisher enc_pub = n.advertise<std_msgs::UInt32>("laser_encoder", 50);
	ros::Publisher diag_pub = n.advertise<laser_spinner::controller_diag>("laser_control_diag", 50);

    //register all the services
    ros::ServiceServer start_service = n.advertiseService("controller/start", start);
    ROS_INFO("Start service initialized");
        
    ros::ServiceServer stop_service = n.advertiseService("controller/stop", stop);
    ROS_INFO("Stop service initialized");

    ros::ServiceServer home_service = n.advertiseService("controller/home", home);
    ROS_INFO("Home service initialized");

    ros::ServiceServer reset_enc_service = n.advertiseService("controller/reset_enc", reset_enc);
    ROS_INFO("Reset Encoder service initialized");

    ros::ServiceServer encodervalue_service = n.advertiseService("controller/encodervalue", encodervalue);
    ROS_INFO("Encoder Value service initialized");

    ros::ServiceServer laser_power_service = n.advertiseService("controller/laser_power", laser_power);
    ROS_INFO("Laser Power service initialized");

	
	ROS_INFO("Opening Serial port");
		
	if(ros::param::has("usb_name")){
		
			ROS_INFO("Detected USB name parameter! Using this..");
			std::string usbname;
			ros::param::get("usb_name", usbname);
			
			sprintf(pathbuffer,"%s%s", fullpath.c_str(), usbname.c_str());
			fullpath = pathbuffer;
	}else{
		ROS_INFO("No USB name parameter detected! Using USB1 as default...");
		sprintf(pathbuffer,"%s%s", fullpath.c_str(), defaultUSBname.c_str());
		fullpath = pathbuffer;
	}
	
	
	printf("%s is the USB path set now!\n", pathbuffer);
	serialport = open(fullpath.c_str(), O_RDWR | O_NOCTTY);
	
	if(serialport < 0){
		perror("Error opening file");
		//printf("OPENING SERIAL FAILED FOR SOME REASON!");
		ROS_ERROR("OPENING SERIAL FAILED FOR SOME REASON!");
		return(1);
	}

	fcntl(serialport, F_SETFL, 0);
	
    //set up the terminal options for baud rates etc.
    tcgetattr((serialport), &options);

    
    options.c_cflag |= (CLOCAL | CREAD);

    /*
     * Set the new options for the port...
     */
    
    //FOR 8N1 style of operation, with no flow control..
    options.c_cflag &= ~CSTOPB; //set stop bits to 1,
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~(CSIZE); //set character size, and no parity
    options.c_cflag |= CS8; //8 data bits
    //options.c_cflag |= HUPCL;  //hang up on last  close
    // NOT SUPPORTED options.c_cflag &= ~(RTSCTS); //disable hardware flow control
    
    
	//options.c_oflag = 0; //turn off all output processing
	
    options.c_lflag &= ~(ICANON | ECHO | IEXTEN | ECHONL | ECHOE | ISIG); //choose raw input mode with associated options
    
    options.c_iflag &= ~(IXON);
    options.c_iflag &= ~(IXOFF);
    options.c_iflag &= ~(ICRNL);
    options.c_iflag &= ~(INLCR);
    options.c_cc[VMIN] = 0;
    options.c_cc[VTIME] = 1; //wait 1/10th second before return from read. this is needed so ROS
                            // and other services can be attended to, if not able to rely on input processes
                            // to trigger these
    
    cfsetispeed(&options, B38400);
    cfsetospeed(&options, B38400);

    if(tcsetattr((serialport), TCSANOW, &options) < 0){
		
			printf("failed to set all the options for TERMIOS!");
			
	} //flush everything, and set options
    
     
    //init the sync clock timestamp. This is needed to begin sending heartbeat to controller
	clock_gettime(CLOCK_MONOTONIC, &last_beat);
	usleep(beat_rate*10); //force an initial delay, so the loop will send the heartbeat first time
	tcflush((serialport), TCIOFLUSH); // flush both in/out buffers.
	
	while( ros::ok() ){

		static unsigned char urxbyte;
		static void *rxbyte;
		unsigned long result;

		urxbyte = 0;
		rxbyte = &urxbyte;
		
		result = read(serialport,(void*)rxbyte, 1);
	    	
      	clock_gettime(CLOCK_MONOTONIC, &current_time); //take current timestamp of robot system

        //this is the heartbeat detect code, if the incoming byte is the heartbeat character, and
        //the transmission is not in the middle of detecting a header sequence, 
        if( (urxbyte == '&') && (!head1_found && !head2_found) ){
            clock_gettime(CLOCK_MONOTONIC, &last_resp); //take current timestamp of robot system
            
            if(!in_sync){
                ROS_INFO("Found/Restored link with controller!");
            }
            in_sync = 1;
            
        }   

		if((head1_found) && (urxbyte == (unsigned char)0xFE)){
			head2_found = 1;
			ROS_DEBUG("PACKET HEADER2 DETECTED!");
				
		}else{
		
		    //if this is the second 0xFF in a row, before a message has started, then it's not a message header!
		    if(urxbyte == 0xFF){
		    head1_found = 0; //double 0xFF in a row means that the read function has timed out and it's
		                    // not actually a message header. This is almost superfluous - but this might reduce
		                    // the likelihood of corrupt packet header coming in and causing problems. 
		    }
		
		}
	
		if((urxbyte == (unsigned char)0xFF) && (!head1_found)){
			head1_found = 1;
			ROS_DEBUG("PACKET HEADER1 DETECTED!");
			
		}	
	
		//IF MESSAGE START BYTES SUCCESSFULLY FOUND, PROCESS MESSAGE AND DO CHECKSUM, BEFORE ROS PUBLISH
		if(head1_found && head2_found){
			
            rxbuffer.clear();
			message_started = 1;
			//ROS_DEBUG("MESSAGE FOUND!");

			
			rxbuffer.push_back((unsigned char)0xFF);
			rxbuffer.push_back((unsigned char)0xFE);			

			
			while(rxbuffer.size() < 15){
			//TODO implement read timeout using sysclock or something
			//to exit this while loop - maybe cable is disconnected?

				result = read(serialport,(void*)rxbyte, 1);
				if(result){
					rxbuffer.push_back(urxbyte); //adds byte to array, increases the size
				}else{
					//continue
				}
			}
			//debug code to see the contents of the byte vector as a serial package
			printf("SERIAL:");

			// print contents of the vector to STDIO for debug purposes
			for (std::vector<unsigned char>::iterator it = rxbuffer.begin();
				it != rxbuffer.end(); ++it){

				sprintf(numbuffer, "%.2X",*it);
			    	printf("%s", numbuffer);
			}
			
			
			static serialmessage cur_message; //make some message structs
			std_msgs::UInt32 encodervalue; // make a ros publish-able message struct
			

			//DO CHECKSUM AND THROW AWAY IF FAIL!
			unsigned char sum = 0;
			
			for(int i = 0; i < (rxbuffer.size() -1); i++){
				
				sum+= rxbuffer[i];
			}
			
			unsigned char sum_compare = rxbuffer[rxbuffer.size()-1]; //the last byte, checksum
			
			//if the checksums do not match, declare failed match and clear message
			if(sum != sum_compare){
				printf("\n\t CHECKSUM FAILED! GOT %.2X, EXPECTED %.2X \n", sum, sum_compare);
				rxbuffer.clear();
				message_started = 0;
				head1_found = 0;
				head2_found = 0;	
				
			//else, if the checksums match
			}else{
								
				// PACK RXBUFFER BYTES INTO THE MESSAGE STRUCT
				pack_message(&cur_message);

				//put message encoder data from struct into the ROS message data contents 
				encodervalue.data = (uint32_t)cur_message.encoder; 
				
				
				lastknownencoder = cur_message.encoder; //save for future reference, and for the rosservice to work
				enc_pub.publish(encodervalue); //publish as topic message
				

				//for debug purposes, show the millisecond timestamp for sanity and data checking
				printf("\tTIME_MS: %u\n", cur_message.time);

				
				//check status flags and publish diagnostics messages/print warnings
				uint8_t statusflags = cur_message.flags;
				laser_spinner::controller_diag diagmsg; // make a ros publish-able message struct for diagnostics
				
				diagmsg.header.seq = msgseq;
				msgseq++;
				
				diagmsg.header.frame_id = "0"; //no frame specifically
				
				//check power good flag bit, and put text into diagmsg etc..
				if(statusflags & (1 << PWR_GOOD)){
					
					diagmsg.power_good = "GOOD";
					
				}else{
					diagmsg.power_good = "TOO LOW!";
					ROS_ERROR("Voltage on the controller input is too low! Need 6V+ for normal operation,"  
								"\n and 11.5V+ for LIDAR and other devices to work properly!"); 
				}
				
				if(statusflags & (1 << MOTOR_STATE)){
					
					diagmsg.motor_state = "MOTOR ENABLED";
					
				}else{
				
					diagmsg.motor_state = "MOTOR DISABLED";
				
				}
				
				//check if the laser power is on or not
				if(statusflags & (1 << LASER_STATE)){
					
					diagmsg.laser_state = "LASER POWER ON";
					
				}else {
					
					diagmsg.laser_state = "LASER POWER OFF";
					
				}
				
				//motor dir bit is 1 for counting 'up' and 0 for 'down'
				if(statusflags & (1 << MOTOR_DIR)){
					if(UP_IS_LEFT){
						diagmsg.direction = "LEFT (CCW)"; //need to work out if "up" means left or right etc.. lol
					}else{
						//up must be right!
						diagmsg.direction = "RIGHT (CW)";
					}
				}else {
					
					if(UP_IS_LEFT){
						//then down must be right!
						diagmsg.direction = "RIGHT (CW)";
					}else{
						diagmsg.direction = "LEFT (CCW)";
						
					}
				}
				
				//check peripherals status flag - usually only useful on start-up
				if(statusflags & (1 << PER_RDY)){
					
					diagmsg.periph_ready = "PERIPHERALS OK";
					
				}else{
					
					diagmsg.periph_ready = "PERIPHERALS FAILED!";
					ROS_ERROR("Peripherals i.e quadrature counter or DAC have failed somehow. check solder joints and test SPI connections");
				}
				
				if(statusflags & (1 << SYNC_SKIP)){
					
					diagmsg.sync_skip = "CONTROLLER CANNOT KEEP UP WITH SYNC RATE!";
					ROS_ERROR("Warning: controller cannot keep up with the interrupt rate from the sensor input");
					ROS_WARN("to send the data fast enough. The maximum rate due to 38400 Baud rate is around 50Hz");
				}else{
					
					diagmsg.sync_skip = "SYNC RATE OK";
				}
				
				
				if(statusflags & (1 << HOME_FAILED)){
					
					diagmsg.home_failed = true;
					ROS_WARN("Hardware failed to find home, should try again. check LEDs etc. related to home functionality");
					
				}else{
					diagmsg.home_failed = false;
				}
				
				diagmsg.speed_clicks_per_sec = cur_message.speed;
				//clicks per sec, to clicks per minute, by rotations per click, gets rotations per minute!
				diagmsg.speed_RPM = (float)(cur_message.speed * 60 * rot_per_click) / (2 * M_PI);
				
				
				diagmsg.header.stamp = ros::Time::now(); //current time for timestamp, done just before sending
				diag_pub.publish(diagmsg); //publish to the diagnostics message topic
				
				
				rxbuffer.clear();
				message_started = 0;
				head1_found = 0;
				head2_found = 0;
					
				
				
			}//end else if checksum match
		} // end message head successfully found.
	

        //maintain link while node is running. Send heartbeat to let controller know the driver node
        //is still active. Also (firstly) check for response timeout
         
        //find difference of time stamps (in milliseconds!) between last received heartbeat response and the current time
        timediff = ( long((current_time.tv_sec - last_resp.tv_sec) * 1000)  + 
                    long((current_time.tv_nsec - last_resp.tv_nsec)/NANOSEC_PER_MILLISEC) );
        
        
        if(in_sync && (timediff > beat_rate_timeout)){

			in_sync = 0;
			ROS_INFO("Node has lost link with controller!");
        }else{
			if(!in_sync && (timediff > beat_rate_timeout*2)){
				ROS_ERROR("Waiting for link to controller...");
				 clock_gettime(CLOCK_MONOTONIC, &last_resp);
				 
			}
			
		}
        
        timediff = 0; //reset variable
        
        //find difference of time stamps in milliseconds to check timeout to send next heartbeat character
        timediff = ( long((current_time.tv_sec - last_beat.tv_sec) * 1000)  + 
                    long((current_time.tv_nsec - last_beat.tv_nsec)/ NANOSEC_PER_MILLISEC) );
        
        if(timediff > beat_rate){
			//std::cout << "sending heartbeat!" << std::endl;

			send_heartbeat();
          
            clock_gettime(CLOCK_MONOTONIC, &current_time);
            last_beat = current_time;
        
        }

		ros::spinOnce();
		
	}
	
	close(serialport);
	
	return 0;
}


//helper function to send heartbeat. called from the heart beat time out condition
void send_heartbeat(void){

    //ROS_INFO("Sending sync heartbeat..");
    int result = write((serialport), "&",1);
	if(result <= 0){
		printf("send failed!\n");
	}
   

    return;
}


//quick checksum that can help detect package failure. This is the sum of all bytes in the message.
//because it's only 8 bits, the value can overflow. This means there are multiple chances of the checksum
//being correct, despite corrupt data. The chances are very low!
uint8_t checksum(uint8_t *writebuffer, int indexToSum){

    uint8_t sum = 0;
    for(int i = 0; i < indexToSum; i++){
    
        sum+=writebuffer[i];
        
    }
    
    return sum;
}

//this function is used to assign values from global variable byte vector into a message struct ptr passed to it.
//it is a helper function to attempt to improve readability of the main code.
void pack_message(struct serialmessage *messagestruct){

	messagestruct->header1 = rxbuffer[0];
	messagestruct->header2 = rxbuffer[1];
	messagestruct->len = rxbuffer[2];
	messagestruct->flags = rxbuffer[3];	
	messagestruct->encoder = 	(rxbuffer[4] << 24) | (rxbuffer[5] << 16) | 
				(rxbuffer[6] << 8) |  (rxbuffer[7]);

	messagestruct->speed =  (rxbuffer[8] << 8) | (rxbuffer[9]);
	messagestruct->time = 	(rxbuffer[rxbuffer.size()-5] << 24) | 
				(rxbuffer[rxbuffer.size()-4] << 16) | 
				(rxbuffer[rxbuffer.size()-3] << 8) | 
				(rxbuffer[rxbuffer.size()-2]);
	messagestruct->checksum = rxbuffer[rxbuffer.size()-1];	

}



// END OF FILE
