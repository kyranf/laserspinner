/*
    \file laser_startup_and_tf.cpp
    
    \brief ROS driver and start-up routine for the sensor spin control ROS node, with continuous transform publishing.
    
    The aim of this program is two-fold: 
        1. initialize the sensor spinning controller to a particular RPM using ROS topic feedback
        2. continue to persist, and act as a transform publisher for the rotating sensor given encoder topic data
    
    This program subscribes to two topics, "laser_encoder" and "laser_control_diag". These are used
    to publish updated transforms of rotation/orientation of the sensor reference frame, and to act as
    an error reporter in some cases - especially during the start-up sequence of the device. 
    
    This program relies heavily on ROS Services as well, to send the set-up sequence to the ROS driver
    for the controller.

    
    \author Kyran Findlater kyfindlater@gmail.com
    Contact: kyfindlater@gmail.com or k.findlater@qut.edu.au
    Date: March-May 2014
*/



#define _USE_MATH_DEFINES
#include <math.h>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <stdint.h>
#include <sys/time.h>
#include <queue>

//include resources such as messages and custom service definitions
#include "std_msgs/UInt32.h"
#include "std_msgs/Bool.h"
#include "laser_spinner/laser_power.h"
#include "laser_spinner/start.h"
#include "laser_spinner/stop.h"
#include "laser_spinner/reset_enc.h"
#include "laser_spinner/home.h"
#include "laser_spinner/encodervalue.h"
#include "laser_spinner/controller_diag.h"

//return error codes from topic loop
#define RETRY 0
#define DONE 1
#define TIMEOUT 2
//number of retries for each service call attempt during set-up
#define NUM_RETRIES 3
//time in seconds before the ROS Topic waiting times out and declares failure to set up.
#define TIMEOUT_SEC 5

//Exception codes
#define POWER_FAILED 0
#define PERIPH_FAILED 1
#define DIAG_TOPIC_TIMEOUT 2
#define HOME_FAILED 3

#define MAX_SPEED_SETTING 60

int current_speed_setting;
int current_dir;

uint32_t last_encoder = 0;
uint32_t home_encoderval = 0;
const uint32_t clicks_per_rev = 29529; // gearbox is 3249/169 ratio, and pulley is 3/1. times the 512 counts per rev on the encoder shaft!
const double radians_per_click = (2*M_PI)/clicks_per_rev;
const uint8_t LEFT = 0;
const uint8_t RIGHT = 1;
const int8_t ROLL_CCW_OR_CW = -1; //-1 says it rolls "backwards" make this 1 if it rolls "forwards" and faces ground instead

ros::Duration recovery_retry_delay = ros::Duration(0.5); //500ms
ros::Duration timeout = ros::Duration(10.0); //seconds
    
const uint16_t START_SPEED = 30;
const float P_GAIN = 0.5;
    
bool waiting_for_home = false; //used to trigger encoder value saving event
bool home_found = false; //used to avoid re-saving any more encoder values as home
bool waiting_for_new = false;
bool firstDiagReceived = false;
bool recovermode = false;

ros::Time lastrecovery;
std::string frame_parent;
std::string frame_childname;

laser_spinner::controller_diag lastDiagnostics;

ros::ServiceClient laserPowerClient;
ros::ServiceClient homeClient;
ros::ServiceClient startClient;
ros::ServiceClient stopClient;
ros::ServiceClient resetEncClient;
ros::ServiceClient encoderClient;
    

std::queue<uint8_t> dirhistory;

inline const char * BoolToString(bool b){
	return b ? "True" : "False";
}


/* 
    \brief Encoder Topic Callback dealing with rotation transforms
    
    Service callback for the Encoder Topic. The topic is defined as a UInt32 message. This callback transforms the encoder data to a rotation transform
    of the laser/sensor reference frame.
    
    \return void
    \note this function is called by ROS topic subscriber, and needs ROS to "Spin".
*/
void encoderCallback(const std_msgs::UInt32 msg){
	
	//ROS_INFO("Laser TF node received encoder data message, transmitting TF..");
	static tf::TransformBroadcaster broadcaster;
	uint32_t encoderval = msg.data;

	last_encoder = encoderval;
	
	//ROS_INFO("Received encoder value of %u", encoderval);
	
	//modulus by clicks_per_rev
	encoderval = encoderval % clicks_per_rev;
	
	//ROS_INFO("post-modulo encoder value of %u", encoderval);
	
	if(waiting_for_home && !home_found){
    
		home_encoderval = encoderval;
		waiting_for_home = false;
		home_found = true;
        
	}
	
	
	double radians;
    
	//offset by home_encoderval;
	//encoderval = (encoderval - home_encoderval) % clicks_per_rev; //re-modulus in case it overflowed backwards
	//multiply by factor of radians per click, to get radians of rotation. then put it into transform and publish it!
	radians = encoderval * radians_per_click;
	radians = radians - M_PI; //sets it back to +- PI radians, rather than  0 - 2PI radians
	
	tf::Transform transform;
	transform.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
	
	tf::Quaternion q;
    
	q.setRPY(radians*ROLL_CCW_OR_CW, 0 , 0); //roll pitch yaw. Because the laser is pitched 'backwards' by 90 degrees, X axis is now the axis of rotation (colinear/parallel to Base_Link's Z axis)
    transform.setRotation(q);
    
    broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/top_of_pole", "orientation_of_laser"));
	
}

/* 
    \brief Diagnostics Topic Callback dealing with Embedded Controller status
    
    Service callback for the Diagnostics Topic. The topic data is defined as a custom diagnostics message. 
    This topic gives periodic feedback which can be used for a 'closed loop' operation of the embedded device, allowing
    code to attempt to change settings or influence it, and see if it worked or not. 
    
    There is also some error handling/warning messages as part of this topic.
    
    \return void
    \note this function is called by ROS topic subscriber, and needs ROS to "Spin".
*/
void diagCallback(const laser_spinner::controller_diag msg){
	
	//listen to the diagnostics feedback, and use this to help the start-up sequencing. 
	//save the message into a private/historical copy, set some flags etc.
	
	if(!firstDiagReceived){
		
		firstDiagReceived = true;
	}

	//save to last diagnostics data
	lastDiagnostics = msg;
	
		
	if(waiting_for_new){
		
		waiting_for_new = false;
	}
	
	//if recovermode is on, (turned on automatically after the init is done, and enters constant ROS Checking loop)
	//then this diagnostics callback will try to detect the RPM of the motor, or Laser power failure, and request
	//the controller to renable the motors to the last known speed setting (the one that caused the good +-1RPM speed)
	//and turn on the laser switch MOSFET
	if(recovermode){
		
		if(laserPowerClient == NULL || startClient == NULL){
			return; //simple initial state check
		}
		
		if((lastDiagnostics.laser_state == "LASER POWER OFF")){
			if(ros::Time::now() - lastrecovery >= recovery_retry_delay){
							
				lastrecovery = ros::Time::now();
				laser_spinner::laser_power laserPowerService;
				laserPowerService.request.state = (uint16_t)1; // set laser power on
				laserPowerClient.call(laserPowerService);
			}
		}
		//if the speed of the motor has dropped below arbitary number (5, in this case) means it's
		//faulted somehow. This can try to auto-recover.
		if((lastDiagnostics.speed_RPM < 5)){
			
			if(ros::Time::now() - lastrecovery >= recovery_retry_delay){
				ROS_ERROR("Warning! RPM has dropped to < 5! attempting to recover automatically!");				
				lastrecovery = ros::Time::now();
				
				laser_spinner::start startService;
				startService.request.speed = (uint16_t)current_speed_setting;
				startService.request.direction = (int16_t)current_dir;
				ROS_INFO("Recovering dropped motor rotation with Speed: %d Dir: %d", current_speed_setting, current_dir);
				startClient.call(startService);

			}
		}
		
	}
	
	// TODO: Code for averaging direction value so that it doesnt toggle so much!
	
	/*
	if(lastDiagnostics.direction == "LEFT"){
		dirhistory.push(LEFT);
	}else{
		dirhistory.push(RIGHT);	
	}
	//if it's got lots of elements in it, pop some off until there is 5
	if(dirhistory.size >= 6){
		while(dirhistory.size != 5){
			dirhistory.pop();
		}
	}
	*/
	return;
	
}//end diag callback


/* 
    \brief Main function, sets up the ROS related code, and then attempts to initialize the laser spinner setup and deal with transform callbacks
    
    The program starts by attempting to initialize the laser spinner, first by making sure its powered on and then attempts to seek for home position.
    once "home", reset encoder value and then a sort of "P" controller attempts to set the speed in RPM given the RPM speed feedback from the controller diagnostics ROS topic. Once settled
    to +-1 RPM the system continues then into an idle state, dealing only with ROS topic callbacks to facilitate transformation of orientation of the sensor due to
    the encoder value referenced from "Home".
    
    \return -1 if the input arguments to the system are not valid. Returns 1 at successful ending of program

 */
int main( int argc, char** argv) {
	ROS_INFO("Starting up Init and TF broadcasting node!");
	ros::init(argc, argv, "laser_startup_and_tf");



	//check input arguments
	if(argc != 5){
		ROS_ERROR("not all arguments given! need speed, direction, and two frame names!");
		return -1;
		
	}
	
	//start ROS related stuff
	
    ros::NodeHandle node;
    ros::service::waitForService("/controller/home", (uint32_t)10000); //by waiting for one, all others should be good too
    homeClient           =   node.serviceClient<laser_spinner::home>("controller/home");
    startClient          =   node.serviceClient<laser_spinner::start>("controller/start");
    stopClient           =   node.serviceClient<laser_spinner::stop>("controller/stop");
	laserPowerClient     =   node.serviceClient<laser_spinner::laser_power>("controller/laser_power");
    resetEncClient       =   node.serviceClient<laser_spinner::reset_enc>("controller/reset_enc");
    encoderClient        =   node.serviceClient<laser_spinner::encodervalue>("controller/encodervalue");
       
       
	//assignment arguments to variables
	int speed = atoi(argv[1]);
	ROS_INFO("Speed input variable is %d rpm \n", speed);
	
	int dir = atoi(argv[2]);
	current_dir = dir; //for global access
	ROS_INFO("direction input variable is %d [1 = left, 0 = right] \n", dir);
	
	frame_parent = argv[3];
	frame_childname = argv[4];
	ROS_INFO("Mapping transform from %s to %s \n", frame_parent.c_str(), frame_childname.c_str());
   
    ros::Subscriber enc_sub = node.subscribe("/laser_encoder", 10, &encoderCallback);
    ros::Subscriber diag_sub = node.subscribe("/laser_control_diag", 10, &diagCallback);
    
    ros::Rate startup_rate(1.0);
    ros::Rate statuspoll_rate(20); 
    
    //try automated set-up routine. if this fails rather than exit the entire program/node
    //just catch and enter the normal transform publishing mode of this program.
    //can decouple this behaviour by making this a seperate node though, perhaps for future work.
    try{
		
		//wait until the first diag message is received, and then attempt standard init process
		while(!firstDiagReceived){
			ROS_INFO("Waiting for first diagnostics/encoder message publish from Controller node...");
			ros::spinOnce();
			startup_rate.sleep();
			
		}
		
		/*
		 *  Standard startup initialization process is:
		 *  - check peripherals and power good flags
		 *	- if above is good, continue on - else throw errors and make user do it manually after fixing errors
		 * 	- turn on laser power, delay 1sec, and call home. if encoder value has changed after 1 second, all is good. if not, resend home command.
		 * 	- maybe do a retry 2-3 times for this sequence
		 * 	- after home is done, send reset encoder command
		 * 	- start with direction = 0, speed at 40. do a 5-10 second control PI loop for trying to achieve 60RPM.
		 */

		//this is where we catch bad initial condition flags - if low power and if peripherals screwed up somehow, abort!
		//sorry about the hard coded string comparisons. Can make a Driver-Wide paramter file for better definitions 
		if(lastDiagnostics.power_good != "GOOD" || lastDiagnostics.periph_ready != "PERIPHERALS OK" ){
			
			ROS_ERROR("ERROR! Power or Peripheral status is fatal on controller! Exiting set-up sequence!");
			
			if(lastDiagnostics.power_good != "GOOD"){ 
				
				throw POWER_FAILED;
			}
			
			if(lastDiagnostics.periph_ready != "PERIPHERALS OK"){ 
				
				throw PERIPH_FAILED;
			}
		}//end if power or peripheral flags are bad
		


		//Code for 'robust' retrying of service calls, with timeouts on topic waiting and max number of retries 
		//if the calls fail (for example, if the controller ignores serial message once or twice)
		int retrycounter = 0;
		int resultcode = 0;
		
		
		do{ //try to turn on laser power

			//call service
			laser_spinner::laser_power laserPowerService;
			laserPowerService.request.state = (uint16_t)1; // set laser power on
			
			if(laserPowerClient.call(laserPowerService))				
				ROS_INFO("Laser power service called, returned: [%s]", BoolToString(laserPowerService.response.state));
			
			
			
			ros::Time starttime = ros::Time::now(); //take a time stamp
			waiting_for_new = true; //set flag to wait for new diag message to come back to us
							
			resultcode = RETRY; //reset state of resultcode variable
			//wait for response, with timeout and setting flag+break on result
			while(1){

				statuspoll_rate.sleep();
				ros::spinOnce();//process callbacks etc.
				
								
				if(ros::Time::now() - starttime >= timeout){
					resultcode = TIMEOUT;
					throw DIAG_TOPIC_TIMEOUT; 

				} 
				
				//if flag is removed by the diagCallBack then have a look at the status flags
				//should be a new message showing changes in state
				if(!waiting_for_new){
			
					if(lastDiagnostics.laser_state != "LASER POWER ON"){
							resultcode = RETRY;
							break;			
					}//end if laser power is not on

					if(lastDiagnostics.laser_state == "LASER POWER ON"){
						ROS_INFO("Laser power confirmed ON, continuing set-up process!");
						resultcode = DONE;
						break;
					}//end if laser is on

				}
				
			}//end while waiting for topic update
			
			retrycounter++;
			
		}while(retrycounter != 3 && resultcode == RETRY);//end of do laser power up call/s
		
		sleep(1); //we assume the laser is on at this point, giving it 1 second to power up,
		//now do the "Home" sequence.
		retrycounter = 0;

		
		do{ //seek home
			
			resultcode = RETRY; //reset state of resultcode variable
			uint32_t encbeforehome = last_encoder; //save the encoder value prior to calling home service
			laser_spinner::home homeservice;
		
			if(homeClient.call(homeservice)){
				ROS_INFO("Call to Home service returned TRUE. Should start seeking home now..");		
			}
			
			ros::Time starttime = ros::Time::now(); //take a time stamp
			waiting_for_new = true; //set flag to wait for new diag message to come back to us
			
			//wait for topic to return information
			while(1){
			
				
				statuspoll_rate.sleep();
				ros::spinOnce();//process callbacks etc.
				
								
				if(ros::Time::now() - starttime >= timeout){
					resultcode = TIMEOUT;
					throw DIAG_TOPIC_TIMEOUT; //throw time out exception.aborts whatever is left of set-up

				}
				
				//if flag is removed by the diagCallBack then have a look at the status flags
				//should be a new message showing changes in state
				if(!waiting_for_new){
					//we have a new message now, lets see if the service call worked!
					
					if(last_encoder == encbeforehome){//check for the encoder value to see if it moved at all
						
						resultcode = RETRY;
						ROS_INFO("laser doesn't seem to have moved at all, trying to seek home again!");
						ROS_WARN("Could be something wrong with motor driver, or serial packages are being lost?"); 
						break;
						
					}
					
					if(lastDiagnostics.home_failed){
						ROS_ERROR("Controller reports failure to find home. trying again!");
						resultcode = RETRY;
						break;
					}else{
						
						ROS_INFO("Found home! resetting encoder to 0..");
						laser_spinner::reset_enc resetservice;
						resetEncClient.call(resetservice);
						resultcode = DONE;
						break;
						
					}
				} //end if got new message, lets see if service call worked
				
			}//end while waiting for topic responses
			
			retrycounter++;
		}while(retrycounter != 3 && resultcode == RETRY);//end of do seek home call/s
		
		ROS_INFO("Should be at home now, about to begin continuous rotation and RPM speed correction");
		
		sleep(1); //wait 1 second while at home
		
		//now start the motor spinning at start rate, followed by closed loop RPM control until settled at set RPM
		retrycounter = 0;
		
		do{ //start up motor
			
			resultcode = RETRY;
			
			laser_spinner::start startService;
			startService.request.speed = START_SPEED;
			startService.request.direction = (int16_t)dir;
			
			startClient.call(startService);
				
			ROS_INFO("Start service called, service returned: [%s]", BoolToString(startService.response.ok));
				
			
			ros::Time starttime = ros::Time::now(); //take a time stamp
			waiting_for_new = true; //set flag to wait for new diag message to come back to us
			
			while(1){ //waiting for topic feedback
				
				resultcode = RETRY; //reset state of resultcode variable
				statuspoll_rate.sleep();
				
				ros::spinOnce();//process callbacks etc.
					
									
				if(ros::Time::now() - starttime >= timeout){
					resultcode = TIMEOUT;
					throw DIAG_TOPIC_TIMEOUT; //throw time out exception.aborts whatever is left of set-up
				}
				
				if(lastDiagnostics.motor_state == "MOTOR ENABLED"){
					resultcode = DONE;
					break;
					
				}else{
					resultcode = RETRY;
					ROS_INFO("Motor doesnt seem to have enabled, trying again!");
					break;
					
				}
			
			}//end while waiting for topic feedback
			
			retrycounter++;
			
		}while(retrycounter != 3 && resultcode == RETRY);

		//if the commands are all working by now, can assume all systems are fine. now attempt to control "speed" value
		//passed to rosservice call "start" every second, to get the desired RPM feedback (seen in the diagnostics topic message)
		ros::Time last_speed_sent = ros::Time::now();
		
		current_speed_setting = START_SPEED;
		ros::Duration control_rate = ros::Duration(2); //how many seconds between each calculation and speed adjustment
		
		bool settled = false;
		bool confirmed_settled = false;

		do{
			
			//if it's time to process value yet
			if(ros::Time::now() - last_speed_sent >= control_rate){
				last_speed_sent = ros::Time::now();
				
				//calculate difference in set speed and RPM feedback value
				int diff = speed - lastDiagnostics.speed_RPM; // speed is "desired rpm"
				
				if(abs(diff) <= 1){
					
					//if the reported RPM is within 1 RPM of the desired value, that's good enough!
					if(!settled){
						settled = true;
						continue;
					}else{
						
						confirmed_settled = true;
						continue;
					}
					
				}
				
				//now lets apply it to the current speed setting and re-send to control board
				if(!diff > 0){
					
					if(abs(diff) > abs(current_speed_setting)){ //if subtracting diff from speed setting will go negative
						
						current_speed_setting = 0; //will not work anyway, but at least it wont
						//try to send a huge unsigned number or a negative number
					}else{//end if subtracting diff from speed setting will go negative
						
						//get absolute value of diff, make it unsigned, and subtract from speed setting
						current_speed_setting-= abs(diff)*P_GAIN; 
							
					
					}
					
				}else{//end if diff is negative 
				
					//diff must be positive, so lets add it to the speed setting and apply it!
					current_speed_setting+=diff*P_GAIN;
					if(current_speed_setting > MAX_SPEED_SETTING){
						current_speed_setting = MAX_SPEED_SETTING;
					}
				}
				
				//apply to controller!
				laser_spinner::start startService;
				startService.request.speed = (uint16_t)current_speed_setting;
				startService.request.direction = (int16_t)dir;
								
				ROS_INFO("\t RPM Controller: Wanted to call Start service with inputs: %u and dir %d", current_speed_setting, dir);
				startClient.call(startService); //sends the service call with desired settings
				
			}//end if it's time to process..
			
			statuspoll_rate.sleep();
			ros::spinOnce();//process callbacks etc.
			
		}while(!confirmed_settled);//end while RPM not settled to within x% of set point
			
		laser_spinner::laser_power laserPowerService;
		laserPowerService.request.state = (uint16_t)1; // set laser power on
		laserPowerClient.call(laserPowerService); //make sure laser power is still on, seems to turn off sometimes..
		
		ROS_INFO("Setup process complete. Sensor should be rotating at required RPM (+-1) and home set");


	//end try block - trying to set up sensor and controller for standard power on, home, and start continuous rotation.
	}catch(int e){
		ROS_ERROR("Auto-init script died, user will have to do the ros service calls manually!");
		if(e == DIAG_TOPIC_TIMEOUT){
			ROS_ERROR("Laser is not turning on properly, or cannot confirm state. Check power, comms and controller node and try again!");
			ROS_ERROR("ROS topic of controller diagnostics failed to be published before timeout");
		}
		if(e == POWER_FAILED){
			ROS_ERROR("Power level (input voltage) too low, cannot continue with init step!"); 
		}
		if(e == PERIPH_FAILED){
			ROS_ERROR("Peripherals (quadrature counter or others) failed somehow! cannot continue with init step!"); 
		}
		
		if(e == HOME_FAILED){
			ROS_ERROR("Failed to find home. Check LED operation and the LDR connections, and the alignment!");
			
		}
		
		ROS_ERROR("You will need to fix the issue/s, and can attempt to call rosservices yourself to \n continue using the controller (which is probably still be running!)");
			
	}
	
	
	ROS_INFO("Initialization complete -- have a good day!");
	
	recovermode = true;
	
    while( node.ok() ){
        
		//just deal with callbacks, now that the set-up and initialization process is over.
		ros::spin();  
        
    }


	return(1);
}//end main


//END OF FILE
