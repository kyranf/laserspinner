FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/laser_spinner/msg"
  "../src/laser_spinner/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_gensrv_lisp"
  "../srv_gen/lisp/encodervalue.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_encodervalue.lisp"
  "../srv_gen/lisp/home.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_home.lisp"
  "../srv_gen/lisp/start.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_start.lisp"
  "../srv_gen/lisp/laser_power.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_laser_power.lisp"
  "../srv_gen/lisp/stop.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_stop.lisp"
  "../srv_gen/lisp/reset_enc.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_reset_enc.lisp"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_gensrv_lisp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
