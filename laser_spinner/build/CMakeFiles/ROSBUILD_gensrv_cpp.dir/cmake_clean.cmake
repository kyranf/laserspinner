FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/laser_spinner/msg"
  "../src/laser_spinner/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_gensrv_cpp"
  "../srv_gen/cpp/include/laser_spinner/encodervalue.h"
  "../srv_gen/cpp/include/laser_spinner/home.h"
  "../srv_gen/cpp/include/laser_spinner/start.h"
  "../srv_gen/cpp/include/laser_spinner/laser_power.h"
  "../srv_gen/cpp/include/laser_spinner/stop.h"
  "../srv_gen/cpp/include/laser_spinner/reset_enc.h"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_gensrv_cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
