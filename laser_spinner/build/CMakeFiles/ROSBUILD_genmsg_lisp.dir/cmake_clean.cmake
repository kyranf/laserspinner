FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/laser_spinner/msg"
  "../src/laser_spinner/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_lisp"
  "../msg_gen/lisp/controller_diag.lisp"
  "../msg_gen/lisp/_package.lisp"
  "../msg_gen/lisp/_package_controller_diag.lisp"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_lisp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
