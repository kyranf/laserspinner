FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/laser_spinner/msg"
  "../src/laser_spinner/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_gensrv_py"
  "../src/laser_spinner/srv/__init__.py"
  "../src/laser_spinner/srv/_encodervalue.py"
  "../src/laser_spinner/srv/_home.py"
  "../src/laser_spinner/srv/_start.py"
  "../src/laser_spinner/srv/_laser_power.py"
  "../src/laser_spinner/srv/_stop.py"
  "../src/laser_spinner/srv/_reset_enc.py"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_gensrv_py.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
