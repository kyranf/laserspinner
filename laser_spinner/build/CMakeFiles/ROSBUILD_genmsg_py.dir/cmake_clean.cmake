FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/laser_spinner/msg"
  "../src/laser_spinner/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_py"
  "../src/laser_spinner/msg/__init__.py"
  "../src/laser_spinner/msg/_controller_diag.py"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_py.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
